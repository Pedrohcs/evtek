<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Paciente extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'cpf', 'nome', 'rg', 'cartao_sus', 'sexo', 'nascimento', 'nome_mae', 'telefone', 'cep', 'avenida_rua',
        'numero', 'quadra', 'lote', 'complemento', 'bairro', 'cidade', 'uf', 'excluido'
    ];

    public static function listaPacientes($paginate, $status)
    {
        if (!$status) {
            $operator = '<>';
        } else {
            $operator = '=';
        }

        $listaPacientes = DB::table('pacientes')
            ->select('pacientes.id', 'pacientes.nome', 'pacientes.nascimento', 'pacientes.cpf')
            ->where('excluido', $operator, '1')
            ->orderBy('pacientes.id', 'DESC')
            ->paginate($paginate);

        return $listaPacientes;
    }
}
