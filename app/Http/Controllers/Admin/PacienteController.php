<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Paciente;
use Illuminate\Support\Facades\DB;

class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function listFilter($status)
    {
        $listaPacientes = Paciente::listaPacientes(2, $status);

        //Calcula a idade
        foreach ($listaPacientes as $paciente => $value) {
            if (!empty($value->nascimento)) {
                list($ano, $mes, $dia) = explode('-', $value->nascimento);
                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                $idade =  floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
                if ($idade == 0.0) {
                    $value->nascimento = '0';
                } else {
                    $value->nascimento = $idade;
                }
            }
        }
        return view('admin.paciente.index', compact('listaPacientes', 'status'));
    }

    public function index()
    {
        $listaPacientes = Paciente::listaPacientes(2, null);
        $status = 0;

        //Calcula a idade
        foreach ($listaPacientes as $paciente => $value) {
            if (!empty($value->nascimento)) {
                list($ano, $mes, $dia) = explode('-', $value->nascimento);
                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                $idade =  floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
                if ($idade == 0.0) {
                    $value->nascimento = '0';
                } else {
                    $value->nascimento = $idade;
                }
            }
        }

        return view('admin.paciente.index', compact('listaPacientes', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ($data['id']) {
            //Atualizar
            $data = $request->all();
            Paciente::find($data['id'])->update($data);
            return redirect()->back();
        } else {
            //Cadastrar
            $data['excluido'] = 0;
            Paciente::create($data);
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Paciente::find($id);
        return $paciente;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $update = DB::table('pacientes')
                ->where('id', $id)
                ->update(['excluido' => 1]);

            if ($update) {
                DB::commit();
                $notification = array(
                    'message' => 'Paciente excluído com sucesso!',
                    'alert' => 'success'
                );
                return redirect()->back();
            } else {
                DB::rollBack();
                $notification = array(
                    'message' => 'Falha ao excluir paciente!',
                    'alert' => 'error'
                );
                return redirect()->back();
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $erro = $e->errorInfo;
            if ($erro[1] == 1451) {
                $notification = array(
                    'message' => 'Registro não pode ser excluído pois está vinculado a outro registro!',
                    'alert' => 'error'
                );
                return redirect()->back();
                return response()->json($notification);
            }
        }
    }
}
