@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <painel titulo="Pacientes">
                <formulario id="formAdicionar" id="$store.state.item.id" css="" action="{{route('paciente.store')}}" method="post" enctype="" token="{{ csrf_token() }}">
                    <div style="display: flex;">
                        <div style="padding-left: 0px;" class="col-md-2">
                            <div class="card" style="width: 10rem;">
                                <center>
                                    <div @drop="onImage(2, $event)" :class="dragEnter ? ['ondrag-enter'] : images[2].content ? ['drop']:['drop', 'ondrag-leave']" @dragover.prevent @dragenter="dragEnter = true" @dragleave="dragEnter = false">
                                        <h4 v-show="!images[2].content">Solte a sua foto aqui</h4>
                                        <img :src="images[2].content" width="120px" ref="resize" />
                                    </div>
                                </center>
                                <div style="text-align: center; padding: 10px;">
                                    <div class="upload-btn-wrapper">
                                        <button class="btn btn-success">
                                            Selecione
                                        </button>
                                        <input type="file" @change="onImage(2, $event)" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <input type="text" style="display: none;" value="$store.state.item.id" class="form-control" style="display:block;" id="id" name="id" v-model="$store.state.item.id">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <input type="text" class="form-control" required id="cpf" name="cpf" v-model="$store.state.item.cpf" placeholder="CPF">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required id="nome" name="nome" v-model="$store.state.item.nome" placeholder="Nome do Paciente">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" required id="rg" name="rg" v-model="$store.state.item.rg" placeholder="RG">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" required id="cartao_sus" name="cartao_sus" v-model="$store.state.item.cartao_sus" placeholder="Cartão SUS">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <select id="sexo" class="form-control" v-model="$store.state.item.sexo" required name="sexo">
                                        <option selected="selected" value=" " disabled>Selecione o sexo</option>
                                        <option value="1">Feminino</option>
                                        <option value="2">Masculino</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="date" class="form-control" required id="nascimento" name="nascimento" v-model="$store.state.item.nascimento" placeholder="Nascimento">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required id="nome_mae" name="nome_mae" v-model="$store.state.item.nome_mae" placeholder="Nome da Mãe">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" required id="telefone" name="telefone" v-model="$store.state.item.telefone" placeholder="Telefone">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <input type="text" class="form-control" required id="cep" name="cep" v-model="$store.state.item.cep" placeholder="CEP">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required id="avenida_rua" name="avenida_rua" v-model="$store.state.item.avenida_rua" placeholder="Avenida/Rua">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" required id="numero" name="numero" v-model="$store.state.item.numero" placeholder="Número">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" required id="quadra" name="quadra" v-model="$store.state.item.quadra" placeholder="Quadra">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" required id="lote" name="lote" v-model="$store.state.item.lote" placeholder="Lote">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <input type="text" class="form-control" required id="complemento" name="complemento" v-model="$store.state.item.complemento" placeholder="Complemento">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" required id="bairro" name="bairro" v-model="$store.state.item.bairro" placeholder="Bairro">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" required id="cidade" name="cidade" v-model="$store.state.item.cidade" placeholder="Cidade">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" required id="uf" name="uf" v-model="$store.state.item.uf" placeholder="UF">
                                </div>
                            </div>
                            <div style="text-align: right;">
                                <button style="margin-bottom: 1%;" id="alt" form="formAdicionar" class="btn btn-info">Atualizar</button>
                                <button style="margin-bottom: 1%;" id="cad" form="formAdicionar" class="btn btn-info">Salvar</button>
                            </div>
                        </div>
                    </div>
                </formulario>
                <ul class="nav nav-tabs">
                    <form style="cursor: pointer;" class="form" id="form1" method="get" action="/admin/lista-paciente/{{0}}" enctype="multipart/form-data">
                        <li class="nav-item">
                            <a class="nav-link {{ $status ==  0 || !$status ? 'active' : ''  }}" onclick=" document.getElementById('form1').submit()">
                                Cadastros Ativos
                            </a>
                        </li>
                    </form>
                    <form style="cursor: pointer;" class="form" method="get" id="form2" action="/admin/lista-paciente/{{1}}" enctype="multipart/form-data">
                        <li class="nav-item">
                            <a class="nav-link {{ $status ==  1 ? 'active' : ''  }}" onclick="document.getElementById('form2').submit()">
                                Cadastros Excluídos
                            </a>
                        </li>
                    </form>
                </ul>
                <lista v-bind:titulos="['#','Nome','Idade','CPF']" v-bind:itens="{{json_encode($listaPacientes)}}" ordem="desc" ordemcol="0" criar="#criar" detalhe="/admin/paciente/" editar="/admin/paciente/" deletar="/admin/paciente/" token="{{ csrf_token() }}" modal="sim"></lista>
                <div style="float: right;">
                    {{$listaPacientes}}
                </div>
            </painel>
        </div>
    </div>
</div>

<script type="application/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>

<script type="application/javascript">
    $(document).ready(function() {
        jQuery(function($) {
            if ($("#id").val() == '') {
                $("#cad").prop('disabled', false);
                $("#alt").prop('disabled', true);
            } else {
                $("#alt").prop('disabled', false);
                $("#cad").prop('disabled', true);
            }

            $("#cpf").mask("000.000.000-00");
            $("#telefone").mask("(00) 0 0000-0000");
            $("#cep").mask("00000-000");
        });
    })
</script>
@endsection