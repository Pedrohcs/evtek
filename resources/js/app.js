/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vuex from 'Vuex';
Vue.use(Vuex);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vuex

const store = new Vuex.Store({
    state: {
        item: {}
    },
    mutations: {
        setItem(state, obj) {
            state.item = obj;
        }
    }
});

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('topo', require('./components/TopoComponent.vue').default);
Vue.component('painel', require('./components/PainelComponent.vue').default);
Vue.component('lista', require('./components/ListaComponent.vue').default);
Vue.component('modallink', require('./components/Modal/ModalLinkComponent.vue').default);
Vue.component('modal', require('./components/Modal/ModalComponent.vue').default);
Vue.component('formulario', require('./components/FormularioComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
    data: {
        content: 'Vue image preview',
        images: new Array(3)
            .fill()
            .map((el, index) => ({ content: null, key: index })),
        dragEnter: false
    },
    methods: {
        onImage(index, e, method) {
            e.stopPropagation();
            e.preventDefault();
            if (index === 2) this.dragEnter = false;
            // check if it's a drop or upload event.
            const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
            if (!file.type.match('image.*')) alert('Select a valid image file');
            // file is too large (more than 1 MB)
            else if (file.size > 1e6) this.resizeImage(index, file, method);
            else this.images[index].content = URL.createObjectURL(file);
        }
    },
    removeImage(index) {
        this.dragEnter = false;
        this.images[index].content = null;
    },
    resizeImage(index, file, method) {
        const reader = new FileReader();
        reader.onload = readerEvent => {
            const img = new Image();
            // Start the image resizing process
            img.onload = () => {
                const { width, height } =
                    method === 1
                        ? this.aspectRatioResize(300)
                        : this.proportionalResize(img.width, img.height);
                const canvas = document.createElement('canvas');
                canvas.width = width;
                canvas.height = height;
                // that's used to draw graphics on the canvas by specifying the position, width and height of the image.
                canvas.getContext('2d').drawImage(img, 0, 0, width, height);
                // Export the canvas as a blob (base64) or DataURL by specifying MIME type, image quality
                this.images[index].content = canvas.toDataURL(
                    'image/jpeg',
                    QUALITY.medium
                );
            };
            img.src = readerEvent.target.result;
        };
        // Read the input image using FileReader (this fires the reader.onload event).
        reader.readAsDataURL(file);
    },
    aspectRatioResize(length) {
        const aspectRatio = Math.round(screen.width / screen.height);
        return {
            width: length / Math.sqrt(1 / Math.pow(aspectRatio, 2) + 1),
            height: length / Math.sqrt(Math.pow(aspectRatio, 2) + 1)
        };
    },
    proportionalResize(width, height) {
        return {
            width: Math.round((MAX_WIDTH / width) * width),
            height: Math.round((MAX_HEIGHT / height) * height)
        };
    },
});
